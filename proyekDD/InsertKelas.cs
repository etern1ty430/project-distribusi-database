﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyekDD
{
    public partial class InsertKelas : Form
    {
        OracleConnection conn;
        String username = "projectdd";
        String password = "123";
        String DB = "SBY";
        String oradb;
        public InsertKelas()
        {
            InitializeComponent();
        }

        private void InsertKelas_Load(object sender, EventArgs e)
        {
            //INGGRIS / MATEMATIKA
            this.Location = new Point(0, 0);
            oradb = "Data Source=" + DB + ";User ID=" + username + ";Password=" + password;
            comboBox1.Text = "Senin";
            refresh();
        }

        private void button1_Click(object sender, EventArgs e)//button back
        {
            ((FormParent)MdiParent).openPegawai();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)//button tambah kelas
        {
            conn.Open();
                string id_jadwal_yang_terpilih = "";
                string jam = "";
                string mapel = "";
                if (radioButton1.Checked)
                {
                    mapel = radioButton1.Text;
                }
                else if (radioButton2.Checked)
                {
                    mapel = radioButton2.Text;
                }
                string grade = textBox2.Text;
                string hari = comboBox1.Text;
                if (numericUpDown1.Value<10)
                {
                    jam = "0"+numericUpDown1.Value.ToString();
                }
                else
                {
                    jam = numericUpDown1.Value.ToString();
                }
                OracleDataAdapter adpt = new OracleDataAdapter();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                if (((FormParent)MdiParent).db == ((FormParent)MdiParent).lokasi_accActive) { cmd = new OracleCommand("SELECT * FROM jadwal where waktu_dan_hari='" + hari + "-" + jam + ".00" + "'", conn); }
                else { cmd = new OracleCommand("SELECT * FROM jadwal@pucukecabang where waktu_dan_hari='" + hari + "-" + jam + ".00" + "'", conn); } 
                OracleDataReader reader = cmd.ExecuteReader();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    id_jadwal_yang_terpilih = reader["KODE_JADWAL"].ToString();
                }
                if (id_jadwal_yang_terpilih=="")
                {
                    //insert jadwal dahulu
                    OracleDataAdapter adpt2 = new OracleDataAdapter();
                    OracleCommand cmd2 = new OracleCommand();
                    cmd2.Connection = conn;
                    if (((FormParent)MdiParent).db == ((FormParent)MdiParent).lokasi_accActive) cmd2 = new OracleCommand("insert into jadwal values('','"+ hari + "-" + jam + ".00" + "')",conn);
                    else cmd2 = new OracleCommand("insert into jadwal@pucukecabang values('','" + hari + "-" + jam + ".00" + "')", conn);
                    cmd2.ExecuteNonQuery();

                    //cari id yang tadi telah dibuat
                    OracleDataAdapter adpt3 = new OracleDataAdapter();
                    OracleCommand cmd3 = new OracleCommand();
                    cmd3.Connection = conn;
                    if (((FormParent)MdiParent).db == ((FormParent)MdiParent).lokasi_accActive) cmd3 = new OracleCommand("SELECT * FROM jadwal where waktu_dan_hari='" + hari + "-" + jam + ".00" + "'", conn);
                    else cmd3 = new OracleCommand("SELECT * FROM jadwal@pucukecabang where waktu_dan_hari='" + hari + "-" + jam + ".00" + "'", conn);
                    OracleDataReader reader3 = cmd3.ExecuteReader();
                    reader3 = cmd.ExecuteReader();
                    while (reader3.Read())
                    {
                        id_jadwal_yang_terpilih = reader3["KODE_JADWAL"].ToString();
                    }

                    //insert kelas setelah insert jadwal
                    OracleDataAdapter adpt4 = new OracleDataAdapter();
                    OracleCommand cmd4 = new OracleCommand();
                    cmd4.Connection = conn;
                    if (((FormParent)MdiParent).db == ((FormParent)MdiParent).lokasi_accActive) cmd4 = new OracleCommand("insert into kelas values('','" + mapel + "','" + grade + "','" + id_jadwal_yang_terpilih + "')", conn);
                    else cmd4 = new OracleCommand("insert into kelas@pucukecabang values('','" + mapel + "','" + grade + "','" + id_jadwal_yang_terpilih + "')", conn);
                    cmd4.ExecuteNonQuery();
                }
                else
                {//insert kelas karena jadwalnya sudah ada
                    OracleDataAdapter adpt2 = new OracleDataAdapter();
                    OracleCommand cmd2 = new OracleCommand();
                    cmd2.Connection = conn;
                    if (((FormParent)MdiParent).db == ((FormParent)MdiParent).lokasi_accActive) cmd2 = new OracleCommand("insert into kelas values('','"+mapel+"','"+grade+"','"+id_jadwal_yang_terpilih + "')", conn);
                    else cmd2 = new OracleCommand("insert into kelas@pucukecabang values('','" + mapel + "','" + grade + "','" + id_jadwal_yang_terpilih + "')", conn);
                    cmd2.ExecuteNonQuery();
                }
                refresh();
                conn.Close();
        }
        public void refresh()
        {
            conn = new OracleConnection(oradb);
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;

            conn.Open();
                if (((FormParent)MdiParent).db == ((FormParent)MdiParent).lokasi_accActive) { cmd.CommandText = "select a.kode_kelas,a.mata_pelajaran,a.grade,b.waktu_dan_hari from kelas a, jadwal b where a.kode_jadwal = b.kode_jadwal order by 1"; }
                else{ cmd.CommandText = "select a.kode_kelas,a.mata_pelajaran,a.grade,b.waktu_dan_hari from kelas@pucukecabang a, jadwal@pucukecabang b where a.kode_jadwal = b.kode_jadwal order by 1"; }
                OracleDataAdapter adpt = new OracleDataAdapter();
                adpt.SelectCommand = cmd;
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                dataGridView1.DataSource = dt;
            conn.Close();
        }
        
    }
}
