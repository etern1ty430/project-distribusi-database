﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyekDD
{
    public partial class Pegawai : Form
    {
        public Pegawai()
        {
            InitializeComponent();
        }

        private void Pegawai_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            label1.Text = "Welcome Pegawai\n" + ((FormParent)MdiParent).nama_accActive;
            if (((FormParent)MdiParent).lokasi_accActive != "pusat") button5.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)//ADD STUDENT
        {
            ((FormParent)MdiParent).openAddSiswa();
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)//LIHAT ABSEN
        {
            ((FormParent)MdiParent).openAbsentPegawai();
            this.Close();
        }
        private void button3_Click(object sender, EventArgs e)//LIHAT HISTORY PEMBELAJARAN
        {
            ((FormParent)MdiParent).openHistoryPembelajaran();
            this.Close();
        }
        private void button4_Click(object sender, EventArgs e)//button logout
        {
            ((FormParent)MdiParent).openLogin();
            this.Close();
        }
        private void button5_Click(object sender, EventArgs e)//TAMBAH SOAL
        {
            ((FormParent)MdiParent).openInsertSoal();
            this.Close();
        }
        private void button6_Click(object sender, EventArgs e)//TAMBAH KELAS
        {
            ((FormParent)MdiParent).openKelas();
            this.Close();
        }
    }
}
