﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class InsertSiswa : Form
    {
        String jenis_log = "";
        OracleConnection conn;
        String username = "projectdd";
        String password = "123";
        String DB = "SBY";
        String oradb;
        public InsertSiswa()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.buttonInsertSiswa = new System.Windows.Forms.Button();
            this.buttonInsertSIswaBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxMatematika = new System.Windows.Forms.CheckBox();
            this.checkBoxInggris = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 113);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(725, 152);
            this.dataGridView1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nama";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(120, 291);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(275, 22);
            this.textBox2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 326);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Email";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(120, 324);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(275, 22);
            this.textBox3.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 355);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(120, 352);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(275, 22);
            this.textBox4.TabIndex = 8;
            // 
            // buttonInsertSiswa
            // 
            this.buttonInsertSiswa.Location = new System.Drawing.Point(120, 392);
            this.buttonInsertSiswa.Name = "buttonInsertSiswa";
            this.buttonInsertSiswa.Size = new System.Drawing.Size(236, 45);
            this.buttonInsertSiswa.TabIndex = 10;
            this.buttonInsertSiswa.Text = "Insert Siswa";
            this.buttonInsertSiswa.UseVisualStyleBackColor = true;
            this.buttonInsertSiswa.Click += new System.EventHandler(this.Button1_Click);
            // 
            // buttonInsertSIswaBack
            // 
            this.buttonInsertSIswaBack.Location = new System.Drawing.Point(12, 12);
            this.buttonInsertSIswaBack.Name = "buttonInsertSIswaBack";
            this.buttonInsertSIswaBack.Size = new System.Drawing.Size(78, 40);
            this.buttonInsertSIswaBack.TabIndex = 12;
            this.buttonInsertSIswaBack.Text = "Back";
            this.buttonInsertSIswaBack.UseVisualStyleBackColor = true;
            this.buttonInsertSIswaBack.Click += new System.EventHandler(this.ButtonInsertSIswaBack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 32);
            this.label1.TabIndex = 11;
            this.label1.Text = "Insert Siswa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(443, 291);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "Kelas : ";
            // 
            // checkBoxMatematika
            // 
            this.checkBoxMatematika.AutoSize = true;
            this.checkBoxMatematika.Location = new System.Drawing.Point(446, 316);
            this.checkBoxMatematika.Name = "checkBoxMatematika";
            this.checkBoxMatematika.Size = new System.Drawing.Size(102, 21);
            this.checkBoxMatematika.TabIndex = 14;
            this.checkBoxMatematika.Text = "Matematika";
            this.checkBoxMatematika.UseVisualStyleBackColor = true;
            // 
            // checkBoxInggris
            // 
            this.checkBoxInggris.AutoSize = true;
            this.checkBoxInggris.Location = new System.Drawing.Point(446, 350);
            this.checkBoxInggris.Name = "checkBoxInggris";
            this.checkBoxInggris.Size = new System.Drawing.Size(124, 21);
            this.checkBoxInggris.TabIndex = 15;
            this.checkBoxInggris.Text = "Bahasa Inggris";
            this.checkBoxInggris.UseVisualStyleBackColor = true;
            // 
            // InsertSiswa
            // 
            this.ClientSize = new System.Drawing.Size(754, 479);
            this.Controls.Add(this.checkBoxInggris);
            this.Controls.Add(this.checkBoxMatematika);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonInsertSIswaBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonInsertSiswa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InsertSiswa";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            oradb = "Data Source=" + DB + ";User ID=" + username + ";Password=" + password;
            //jenis log disini

            // ada di owner@sby/ownerR
            refresh();
        }

        public void refresh()
        {
            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "SELECT * FROM siswa ORDER BY KODE_SISWA ASC";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "SELECT * FROM siswa@pucukecabang ORDER BY KODE_SISWA ASC";
            adpt.SelectCommand = cmd;
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            dataGridView1.DataSource = dt;
            conn.Close();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            checkBoxInggris.Checked = false;
            checkBoxMatematika.Checked = false;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            String nama = textBox2.Text;
            String email = textBox3.Text;
            String pass= textBox4.Text;

            conn = new OracleConnection(oradb);
            conn.Open();
            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            if(((FormParent)MdiParent).db == "pusat") cmd.CommandText = "INSERT INTO SISWA(KODE_SISWA,NAMA,EMAIL,PASSWORD) VALUES('','"+nama+"','"+email+"','"+pass+"')";
            else if(((FormParent)MdiParent).db == "cabang") cmd.CommandText = "INSERT INTO SISWA@pucukecabang(KODE_SISWA,NAMA,EMAIL,PASSWORD) VALUES('','" + nama + "','" + email + "','" + pass + "')";
            cmd.ExecuteNonQuery();

            String query_siswa = "";
            if (((FormParent)MdiParent).db == "pusat") query_siswa = "SELECT * FROM SISWA WHERE nama='" + nama + "' and email='" + email + "' and password='" + pass + "'";
            else if (((FormParent)MdiParent).db == "cabang") query_siswa = "SELECT * FROM SISWA@pucukecabang WHERE nama='" + nama + "' and email='" + email + "' and password='" + pass + "'";

            OracleCommand cmd_siswa = new OracleCommand();
            cmd_siswa.Connection = conn;
            cmd_siswa.CommandText = query_siswa;
            String kode_Siswa = cmd_siswa.ExecuteScalar().ToString();
            Console.WriteLine(kode_Siswa);
            //insert MAPEL
            if (checkBoxMatematika.Checked)
            {
                OracleDataAdapter adpt_mat = new OracleDataAdapter();
                OracleCommand cmd_mat = new OracleCommand();
                cmd.Connection = conn;
                if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "INSERT INTO PELAJARAN(KODE_SISWA,MATA_PELAJARAN,GRADE,CTRLES) VALUES('" + kode_Siswa + "','MATEMATIKA','A','0')";
                else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "INSERT INTO PELAJARAN@pucukecabang(KODE_SISWA,MATA_PELAJARAN,GRADE,CTRLES) VALUES('" + kode_Siswa + "','MATEMATIKA','A','0')";
                cmd.ExecuteNonQuery();
            }
            if (checkBoxInggris.Checked)
            {
                OracleDataAdapter adpt_mat = new OracleDataAdapter();
                OracleCommand cmd_mat = new OracleCommand();
                cmd.Connection = conn;
                if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "INSERT INTO PELAJARAN(KODE_SISWA,MATA_PELAJARAN,GRADE,CTRLES) VALUES('" + kode_Siswa + "','INGGRIS','A','0')";
                else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "INSERT INTO PELAJARAN@pucukecabang(KODE_SISWA,MATA_PELAJARAN,GRADE,CTRLES) VALUES('" + kode_Siswa + "','INGGRIS','A','0')";
                cmd.ExecuteNonQuery();
            }
            refresh();
            MessageBox.Show("Insert Siswa Berhasil!!!");
        }

        private void ButtonInsertSIswaBack_Click(object sender, EventArgs e)
        {
            ((FormParent)MdiParent).openPegawai();
            this.Close();

            //ke pegawai cabang
            if (jenis_log == "pemcab") { }
            //ke pegawai pusat
            else { }
        }
    }
}
