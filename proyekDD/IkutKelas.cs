﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace proyekDD
{
    public partial class IkutKelas : Form
    {
        OracleConnection conn = new OracleConnection("Data Source=sby;User ID=projectdd;Password=123");
        List<string> listPelajaran = new List<string>();
        string gradeMat = ""; string gradeIng = "";

        string kelasPilihan = "";
        string soalPilihan = "";
        string kodeAcc = "";
        string namaSoal = "";

        string mapelPilihan = "";


        //VAR BUAT TAMPILIN KELAS KOSONG
        List<string> list_kelas_yang_kosong = new List<string>();

        List<string> listkode = new List<string>();
        List<string> listmapel = new List<string>();
        List<string> listgrade = new List<string>();
        List<string> listjadwal = new List<string>();


        List<string> listsoal = new List<string>();
        List<string> listkelas = new List<string>();
        List<string> listmateri = new List<string>();
        List<string> listpegawai = new List<string>();

        public IkutKelas()
        {
            InitializeComponent();
        }
        
        private void IkutKelas_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            kodeAcc = ((FormParent)MdiParent).kode_accActive;
            panel1.Visible = false; label2.Visible = false; labelAbsent.Visible = false; button2.Visible = false; label6.Visible = false;
            cekPelajaran();
        }

        public void cekPelajaran() {
            listPelajaran.Clear();
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from tab");
            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select * from pelajaran where kode_siswa='" + ((FormParent)MdiParent).kode_accActive + "'", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select * from pelajaran@pucukecabang where kode_siswa='" + ((FormParent)MdiParent).kode_accActive + "'", conn);
            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                listPelajaran.Add(reader["mata_pelajaran"].ToString());
                if (reader["mata_pelajaran"].ToString() == "MATEMATIKA") gradeMat = reader["grade"].ToString();
                else if (reader["mata_pelajaran"].ToString() == "INGGRIS") gradeIng = reader["grade"].ToString();
            }
            conn.Close();

            radioButton1.Enabled = false;
            radioButton2.Enabled = false;
            for (int i = 0; i < listPelajaran.Count; i++)
            {
                if (listPelajaran[i] == "MATEMATIKA") radioButton1.Enabled = true;
                if (listPelajaran[i] == "INGGRIS") radioButton2.Enabled = true;
            }
        }//ENABLE RADIO SESUAI MAPEL YANG DIIKUTI dan ISI GRADENYA
        public void refreshDGV(string mapel) {
            //conn.Open();
            //OracleDataAdapter adpt = new OracleDataAdapter();
            //OracleCommand cmd = new OracleCommand();
            //cmd.Connection = conn;

            //cmd.CommandText = "select k.kode_kelas,k.mata_pelajaran,k.grade,j.waktu_dan_hari from kelas k,jadwal j where k.mata_pelajaran='" + mapel + "' and k.kode_jadwal=j.kode_jadwal";
            //adpt.SelectCommand = cmd;
            //DataTable dt = new DataTable();
            //adpt.Fill(dt);
            //dataGridView1.DataSource = dt;
            //conn.Close();

            conn.Open();
            OracleDataAdapter adpt2 = new OracleDataAdapter();
            OracleCommand cmd2 = new OracleCommand();
            cmd2.Connection = conn;

            OracleDataAdapter adpt3 = new OracleDataAdapter();
            OracleCommand cmd3 = new OracleCommand();
            cmd3.Connection = conn;

            OracleDataAdapter adpt4 = new OracleDataAdapter();
            OracleCommand cmd4 = new OracleCommand();
            cmd4.Connection = conn;

            cmd2 = new OracleCommand("SELECT * FROM KELAS", conn);
            OracleDataReader reader2 = cmd2.ExecuteReader();
            reader2 = cmd2.ExecuteReader();
            while (reader2.Read())
            {
                listkode.Add(reader2["kode_kelas"].ToString());
                listmapel.Add(reader2["mata_pelajaran"].ToString());
                listgrade.Add(reader2["grade"].ToString());
                listjadwal.Add(reader2["kode_jadwal"].ToString());
            }

            if (((FormParent)MdiParent).db == "pusat") { cmd3 = new OracleCommand("SELECT * FROM soal", conn); }
            else if (((FormParent)MdiParent).db == "cabang") { cmd3 = new OracleCommand("SELECT * FROM soal@pucukecabang", conn); }

            OracleDataReader reader3 = cmd3.ExecuteReader();
            reader3 = cmd3.ExecuteReader();
            while (reader3.Read())
            {
                listsoal.Add(reader3["kode_soal"].ToString());
                listkelas.Add(reader3["kode_kelas"].ToString());
                listmateri.Add(reader3["judul_materi"].ToString());
                listpegawai.Add(reader3["kode_pegawai"].ToString());
            }
            string hasil = "";
            foreach (var item in listkode)
            {
                bool berhasil = true;
                foreach (var item2 in listkelas)
                {
                    if (item == item2)
                    {
                        berhasil = false;
                    }
                }
                if (!berhasil)
                {
                    list_kelas_yang_kosong.Add(item);
                    if (hasil == "")
                    {
                        hasil = hasil + "a.kode_kelas='" + item + "'";
                    }
                    else
                    {
                        hasil = hasil + " or a.kode_kelas='" + item + "'";
                    }
                }
            }
            if (hasil != "")
            {
                if (mapel == "MATEMATIKA") cmd4.CommandText = "select a.kode_kelas,a.mata_pelajaran,a.grade,b.waktu_dan_hari from kelas a, jadwal b where (" + hasil + ") and a.kode_jadwal = b.kode_jadwal and a.mata_pelajaran='" + mapel + "' and a.grade='" + gradeMat + "' order by 1";
                else if (mapel == "INGGRIS") cmd4.CommandText = "select a.kode_kelas,a.mata_pelajaran,a.grade,b.waktu_dan_hari from kelas a, jadwal b where (" + hasil + ") and a.kode_jadwal = b.kode_jadwal and a.mata_pelajaran='" + mapel + "' and a.grade='" + gradeIng + "' order by 1";
                adpt4.SelectCommand = cmd4;
                DataTable dt4 = new DataTable();
                adpt4.Fill(dt4);
                dataGridView1.DataSource = dt4;
            }

            conn.Close();
        }
        public void refreshAbsent(string mapel) {
            int pengurangan = -1;
            if ((int)DateTime.Today.DayOfWeek==0) { pengurangan = 7; }
            else { pengurangan = (int)DateTime.Today.DayOfWeek; }
            DateTime monday = DateTime.Today.AddDays(-pengurangan);
            monday = monday.AddDays(1);
            //MessageBox.Show(monday.ToString("d"));

            DateTime sundaynext = DateTime.Today.AddDays(+(7 -pengurangan));
            //MessageBox.Show(sundaynext.ToString("d"));

            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from tab");
            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select count(*) from log_absensi where kode_siswa='" + ((FormParent)MdiParent).kode_accActive + "' and mata_pelajaran='" + mapel + "' and tgl>=to_date('" + monday.ToString("d") + "','DD-MM-YYYY') and tgl<=to_date('" + sundaynext.ToString("d") + "','DD-MM-YYYY')", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select count(*) from log_absensi@pucukecabang where kode_siswa='" + ((FormParent)MdiParent).kode_accActive + "' and mata_pelajaran='" + mapel + "' and tgl>=to_date('" + monday.ToString("d") + "','DD-MM-YYYY') and tgl<=to_date('" + sundaynext.ToString("d") + "','DD-MM-YYYY')", conn);
            string hasil = cmd.ExecuteScalar().ToString();
            labelAbsent.Text = hasil;

            Console.WriteLine("select count(*) from log_absensi where kode_siswa='" + ((FormParent)MdiParent).kode_accActive + "' and mata_pelajaran='" + mapel + "' and tgl>=to_date('" + monday.ToString("d") + "','DD-MM-YYYY') and tgl<=to_date('" + sundaynext.ToString("d") + "','DD-MM-YYYY')");

            conn.Close();

            if (Convert.ToInt32(hasil) == 2)
            {
                dataGridView1.Enabled = false;
            }
            else dataGridView1.Enabled = true;
        }//MENAMPILKAN BANYAK ABSEN DALAM SEMINGGU SESUAI PELAJARAN YANG DIPILIH
        public void cariSoal() {
            conn.Open();
            OracleCommand cmd = new OracleCommand("select * from tab");
            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select judul_materi from soal where kode_kelas='" + kelasPilihan + "'", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select judul_materi from soal@pucukecabang where kode_kelas='" + kelasPilihan + "'", conn);
            namaSoal = cmd.ExecuteScalar().ToString();

            if (((FormParent)MdiParent).db == "pusat") cmd = new OracleCommand("select kode_soal from soal where kode_kelas='" + kelasPilihan + "'", conn);
            else if (((FormParent)MdiParent).db == "cabang") cmd = new OracleCommand("select kode_soal from soal@pucukecabang where kode_kelas='" + kelasPilihan + "'", conn);
            soalPilihan = cmd.ExecuteScalar().ToString();
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ((FormParent)MdiParent).openSiswa();
            this.Close();
        }//BUTTON KEMBALI KE MENU SISWA
        private void button2_Click(object sender, EventArgs e)
        {
            conn.Open();

            OracleDataAdapter adpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();

            //insert history siswa
            cmd.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "INSERT INTO history_siswa VALUES('','','" + kodeAcc + "','" + kelasPilihan + "','" + soalPilihan + "', '', 'pending')";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "INSERT INTO history_siswa@pucukecabang VALUES('','','" + kodeAcc + "','" + kelasPilihan + "','" + soalPilihan + "', '', 'pending')";
            cmd.ExecuteNonQuery();

            //insert log absensi
            cmd.Connection = conn;
            if (((FormParent)MdiParent).db == "pusat") cmd.CommandText = "INSERT INTO log_absensi VALUES('" + kodeAcc + "',TO_DATE('" + DateTime.Now.ToString("d") + "', 'dd-mm-yyyy'),'" + mapelPilihan + "')";
            else if (((FormParent)MdiParent).db == "cabang") cmd.CommandText = "INSERT INTO log_absensi@pucukecabang VALUES('" + kodeAcc + "',TO_DATE('" + DateTime.Now.ToString("d") + "', 'dd-mm-yyyy'),'" + mapelPilihan + "')";
            cmd.ExecuteNonQuery();

            conn.Close();
            MessageBox.Show("IKUTI KELAS BERHASIL");
            refreshAbsent(mapelPilihan);
        }//BUTTON PILIH KELAS DAN ABSEN

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            panel1.Visible = true; label2.Visible = true; labelAbsent.Visible = true; label6.Visible = true;
            if (radioButton1.Checked)
            {
                mapelPilihan = "MATEMATIKA";
                refreshDGV(radioButton1.Text.ToUpper());
                refreshAbsent(radioButton1.Text.ToUpper());
                label6.Text = "Grade : " + gradeMat;
            }
            else if (radioButton2.Checked)
            {
                mapelPilihan = "INGGRIS";
                refreshDGV(radioButton2.Text.ToUpper());
                refreshAbsent(radioButton2.Text.ToUpper());
                label6.Text = "Grade : " + gradeIng;
            }

        }//PILIH MATAPELAJARAN MATEMATIKA
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            panel1.Visible = true; label2.Visible = true; labelAbsent.Visible = true; label6.Visible = true;
            if (radioButton1.Checked)
            {
                mapelPilihan = "MATEMATIKA";
                refreshDGV(radioButton1.Text.ToUpper());
                refreshAbsent(radioButton1.Text.ToUpper());
                label6.Text = "Grade : " + gradeMat;
            }
            else if (radioButton2.Checked)
            {
                mapelPilihan = "INGGRIS";
                refreshDGV(radioButton2.Text.ToUpper());
                refreshAbsent(radioButton2.Text.ToUpper());
                label6.Text = "Grade : " + gradeIng;
            }
        }//PILIH MATAPELAJARAN INGGRIS


        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            kelasPilihan = dataGridView1[0, e.RowIndex].Value.ToString();
            cariSoal();
            label3.Text = "Kelas : " + dataGridView1[0, e.RowIndex].Value.ToString();
            label4.Text = "Materi Soal : " + namaSoal;
            label5.Text = "Jadwal : " + dataGridView1[3, e.RowIndex].Value.ToString();
            button2.Visible = true;
        }
        
    }
}
