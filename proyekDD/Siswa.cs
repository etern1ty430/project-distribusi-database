﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyekDD
{
    public partial class Siswa : Form
    {
        public Siswa()
        {
            InitializeComponent();
        }

        private void Siswa_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            label1.Text = "Welcome,\n" + ((FormParent)MdiParent).nama_accActive;
        }

        private void button1_Click(object sender, EventArgs e)//BUTTON LOGOUT
        {
            ((FormParent)MdiParent).openLogin();
            this.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ((FormParent)MdiParent).openEditProfile();
            this.Close();
        }//BUTTON EDIT PROFILE
        private void button3_Click(object sender, EventArgs e)
        {
            ((FormParent)MdiParent).openIkutKelas();
            this.Close();
        }//BUTTON UNTUK MEMILIH KELAS
    }
}
